package com.ruben.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.ruben.interfaces.ListDataListener;

import java.util.List;

/**
 * @ClassName: LinearRecyclerView
 * @Description: 我还没有写描述
 * @Date: 2020/10/17 0017 1:52
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class LinearRecyclerView extends RecyclerView {
    private SwipeRefreshLayout refreshLayout;
    private LinearLayoutManager linearLayoutManager;
    private ListDataListener listDataListener;
    private boolean loading = false;
    private List<View> headerViewList;
    private int scrollY = 0;
    private boolean isFirst = true;
    private boolean isChargeLoad = true;

    public LinearRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void setListDataListener(ListDataListener listDataListener) {
        this.listDataListener = listDataListener;
    }

    /**
     * @MethodName: loadFinish
     * @Description: 我还没有写描述
     * @Date: 2020/10/17 0017 1:59
     * *
     * @author: <achao1441470436@gmail.com>
     * @param: [type]
     * @returnValue: void
     */
    public void loadFinish(int type) {
        loading = false;
        if (refreshLayout != null) {
            refreshLayout.setEnabled(true);
        }
    }

    public void refreshFinish() {
        loading = false;
        refreshLayout.setRefreshing(true);
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public void loading() {
        if (!refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(true);
        }
    }

    public void setLinearLayoutManager() {
        this.linearLayoutManager = new LinearLayoutManager(this.getContext());
        this.setLayoutManager(this.linearLayoutManager);
    }

    public void setLinearLayoutManagerReverse() {
        linearLayoutManager = new LinearLayoutManager(this.getContext());
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);
        this.setLayoutManager(linearLayoutManager);
    }

    public void setRefreshLayout(final SwipeRefreshLayout refreshLayout) {
        if (refreshLayout == null) {
            return;
        }
        this.refreshLayout = refreshLayout;
        this.refreshLayout.setColorSchemeColors(0xff238ACB);
        this.refreshLayout.setOnRefreshListener(() -> {

            if (listDataListener != null && !loading) {
                loading = true;
                listDataListener.refresh();
            }
        });
    }

    @Override
    public void onScrolled(int dx, int dy) {
        super.onScrolled(dx, dy);
        scrollY += dy;
        try {
            int endCompletelyPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
            if (isChargeLoad) {
                if (dy <= 0 || loading) {
                    return;
                } else {
                    if (endCompletelyPosition == getAdapter().getItemCount() - 1 && !loading && null != listDataListener) {
                        loading = true;
                        refreshLayout.setEnabled(false);
                        listDataListener.loadMore();
                    }
                }
                return;
            }
            if (dy <= 0) {
                return;
            }
            if (endCompletelyPosition == getAdapter().getItemCount() - 1 && !loading && null != listDataListener) {
                loading = true;
                if (refreshLayout != null) {
                    refreshLayout.setEnabled(false);
                }
                listDataListener.loadMore();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onScrollStateChanged(int state) {
        super.onScrollStateChanged(state);
        try {
            if (state != 0) {
                Glide.with(LinearRecyclerView.this.getContext()).pauseRequests();
            } else {
                Glide.with(LinearRecyclerView.this.getContext()).resumeRequests();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
