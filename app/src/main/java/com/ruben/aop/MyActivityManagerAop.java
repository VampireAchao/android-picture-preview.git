package com.ruben.aop;

import android.app.Activity;
import android.app.Application;
import android.util.Log;

import com.ruben.manager.MyActivityLifecycleCallbacks;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import lombok.SneakyThrows;

/**
 * @ClassName: MyActivityManagerAop
 * @Description: 我还没有写描述
 * @Date: 2020/10/18 0018 11:40
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Aspect
public class MyActivityManagerAop {

    @Pointcut("execution(* com.ruben..*.onCreate(..))")
    public void onCreatePointcut() {

    }

    @SneakyThrows
    @Around("onCreatePointcut()")
    public Object addActivity(final ProceedingJoinPoint joinPoint) {
        Object object = joinPoint.getTarget();
        if (!(object instanceof Activity)) {
            return joinPoint.proceed();
        }
        Activity activity = (Activity) object;
        Application application = activity.getApplication();
        MyActivityLifecycleCallbacks myActivityLifecycleCallbacks = new MyActivityLifecycleCallbacks();
        application.registerActivityLifecycleCallbacks(myActivityLifecycleCallbacks);
        Log.i(activity.getClass().getName(), "aop execute correct");
        return joinPoint.proceed();
    }
}
