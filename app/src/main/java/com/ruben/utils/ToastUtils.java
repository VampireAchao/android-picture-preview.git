package com.ruben.utils;

import android.widget.Toast;

/**
 * @ClassName: ToastUtils
 * @Description: 我还没有写描述
 * @Date: 2020/10/18 0018 10:08
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class ToastUtils {
    public static void shortToast(String value) {
        toast(value, Toast.LENGTH_SHORT);
    }

    public static void longToast(String value) {
        toast(value, Toast.LENGTH_LONG);
    }

    private static void toast(String value, int length) {
        Toast.makeText(MyActivityManager.getActivity(), value, length).show();
    }
}
