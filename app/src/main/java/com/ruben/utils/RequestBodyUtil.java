package com.ruben.utils;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * @ClassName: RequestBodyUtil
 * @Description: 我还没有写描述
 * @Date: 2020/10/17 0017 2:39
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class RequestBodyUtil {
    /**
     * @MethodName: getTextBody
     * @Description: 我还没有写描述
     * @Date: 2020/10/17 0017 2:40
     * *
     * @author: <achao1441470436@gmail.com>
     * @param: [body]
     * @returnValue: okhttp3.RequestBody
     */
    public static RequestBody getTextBody(String body) {
        return RequestBody.create(MediaType.parse("text/plain"), body);
    }
}
