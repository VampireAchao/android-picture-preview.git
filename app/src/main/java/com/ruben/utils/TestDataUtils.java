package com.ruben.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: TestDataUtils
 * @Description: 我还没有写描述
 * @Date: 2020/10/18 0018 10:03
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class TestDataUtils {
    public List<String> getFakeImages() {
        List<String> imageList = new ArrayList<>();
        imageList.add("http://p3.qhimg.com/bdm/1600_900_85/t01c5a1167f950bde7e.jpg");
        imageList.add("http://p1.qhimg.com/bdm/1600_900_85/t01e82b2196d41687f9.jpg");
        imageList.add("http://p3.qhimg.com/bdm/1600_900_85/t01d2383ddfecff3a12.jpg");
        imageList.add("http://p6.qhimg.com/bdm/1600_900_85/t01d71361ec7df239bc.jpg");
        imageList.add("http://p2.qhimg.com/bdm/1600_900_85/t01187ddecda9a4cf2c.jpg");
        imageList.add("http://p9.qhimg.com/bdm/1600_900_85/t01c91d12b10e40ddee.jpg");
        imageList.add("http://p4.qhimg.com/bdm/1600_900_85/t01bb1e922d69e7be9b.jpg");
        imageList.add("http://p6.qhimg.com/bdm/1600_900_85/t01807223558e03a1ce.jpg");
        imageList.add("http://p9.qhimg.com/bdm/1600_900_85/t014a17887bbb8fe197.jpg");
        imageList.add("http://p0.qhimg.com/bdm/1600_900_85/t01262144660ded6ee9.jpg");
        imageList.add("http://p8.qhimg.com/bdm/1600_900_85/t01f1e0d0c1069742ef.jpg");
        imageList.add("http://p8.qhimg.com/bdm/1600_900_85/t01f50e8b2ba8e2c260.jpg");
        imageList.add("http://p7.qhimg.com/bdm/1600_900_85/t0106887d52923fb708.jpg");
        imageList.add("http://p9.qhimg.com/bdm/1600_900_85/t01383ab178e2aaae5c.jpg");
        imageList.add("http://p3.qhimg.com/bdm/1600_900_85/t0132ad520533a32ebc.jpg");
        imageList.add("http://p8.qhimg.com/bdm/1600_900_85/t0176406bd1d97dd32b.jpg");
        imageList.add("http://p0.qhimg.com/bdm/1600_900_85/t017a0140421e6ac2cb.jpg");
        imageList.add("http://p0.qhimg.com/bdm/1600_900_85/t01a972cfbfa327a70e.jpg");
        imageList.add("http://p3.qhimg.com/bdm/1600_900_85/t015d3cd9fa07f2d7a4.jpg");
        imageList.add("http://p3.qhimg.com/bdm/1600_900_85/t01b5956887273affa5.jpg");
        imageList.add("http://p4.qhimg.com/bdm/1600_900_85/t01d2806252ec3098b6.jpg");
        imageList.add("http://p1.qhimg.com/bdm/1600_900_85/t015f5830253139a848.jpg");
        imageList.add("http://p5.qhimg.com/bdm/1600_900_85/t017ab9a89fa2d9c026.jpg");
        imageList.add("http://p8.qhimg.com/bdm/1600_900_85/t013d9edca1c249aa2f.jpg");
        imageList.add("http://p4.qhimg.com/bdm/1600_900_85/t01e868c42c846a63b4.jpg");
        imageList.add("http://p8.qhimg.com/bdm/1600_900_85/t01659e0702e7e8b2f9.jpg");
        imageList.add("http://p0.qhimg.com/bdm/1600_900_85/t01582af447d0aa4805.jpg");
        imageList.add("http://p7.qhimg.com/bdm/1600_900_85/t0186e37a47bab90cd1.jpg");
        imageList.add("http://p4.qhimg.com/bdm/1600_900_85/t01686f7c28382e8430.jpg");
        imageList.add("http://p1.qhimg.com/bdm/1600_900_85/t0107c950fcb8f54e90.jpg");
        return imageList;
    }
}
